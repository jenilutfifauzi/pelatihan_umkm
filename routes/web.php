<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login'); //redirect ke halaman login
});

Auth::routes();

//FEATURES
Route::resource('kelola_user', App\Http\Controllers\Features\KelolaUserController::class);
// pelatihan
Route::resource('pelatihan', App\Http\Controllers\Features\PelatihanController::class);
// jadwal pelatihan
Route::resource('jadwal', App\Http\Controllers\Features\JadwalPelatihanController::class);
// Materi
Route::resource('kelola_materi', App\Http\Controllers\Features\KelolaMateriController::class);
//user pelatihan
Route::post('pelatihan_user/user_register/{id}', [App\Http\Controllers\Features\PelatihanUserController::class, 'user_register'])->name('pelatihan_user.user_register');
Route::resource('pelatihan_user', App\Http\Controllers\Features\PelatihanUserController::class);
//user materi
Route::get('materi_user', [App\Http\Controllers\Features\MateriUserController::class, 'index'])->name('materi_user.index');
Route::get('materi_user/{id}', [App\Http\Controllers\Features\MateriUserController::class, 'show'])->name('materi_user.show');
Route::get('materi_user/{id}/detail', [App\Http\Controllers\Features\MateriUserController::class, 'detail'])->name('materi_user.detail');
//kehadiran user
Route::post('kehadiran_user', [App\Http\Controllers\Features\KehadiranUserController::class, 'store'])->name('kehadiran_user.store');
//komentar user
Route::post('komentar_user', [App\Http\Controllers\Features\KomentarUserController::class, 'store'])->name('komentar_user.store');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// admin

Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::get('admin/dashboard', function () {
        return view('dashboard/index');
    })->name('admin.dashboard');

    Route::get('admin/profile', function () {
        return "Admin Profile";
    })->name('admin.profile');
});

// mentor
Route::group(['middleware' => ['auth']], function () {
    Route::get('mentor/dashboard', function () {
        return view('dashboard/index');
    })->name('mentor.dashboard');
});

// user
Route::group(['middleware' => ['auth']], function () {
    Route::get('user/dashboard', function () {
        return view('dashboard/index');
    })->name('user.dashboard');
});