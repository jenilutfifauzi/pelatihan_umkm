-- MySQL dump 10.13  Distrib 8.0.35, for Linux (x86_64)
--
-- Host: localhost    Database: pelatihan_umkm
-- ------------------------------------------------------
-- Server version	8.0.35-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dokumen_pendukungs`
--

DROP TABLE IF EXISTS `dokumen_pendukungs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dokumen_pendukungs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_pelatihan` bigint unsigned NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dokumen_pendukungs_id_pelatihan_foreign` (`id_pelatihan`),
  CONSTRAINT `dokumen_pendukungs_id_pelatihan_foreign` FOREIGN KEY (`id_pelatihan`) REFERENCES `pelatihans` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dokumen_pendukungs`
--

LOCK TABLES `dokumen_pendukungs` WRITE;
/*!40000 ALTER TABLE `dokumen_pendukungs` DISABLE KEYS */;
/*!40000 ALTER TABLE `dokumen_pendukungs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jadwal_pelatihans`
--

DROP TABLE IF EXISTS `jadwal_pelatihans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jadwal_pelatihans` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_pelatihan` bigint unsigned NOT NULL,
  `id_user` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jadwal_pelatihans_id_pelatihan_foreign` (`id_pelatihan`),
  KEY `jadwal_pelatihans_id_user_foreign` (`id_user`),
  CONSTRAINT `jadwal_pelatihans_id_pelatihan_foreign` FOREIGN KEY (`id_pelatihan`) REFERENCES `pelatihans` (`id`) ON DELETE CASCADE,
  CONSTRAINT `jadwal_pelatihans_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jadwal_pelatihans`
--

LOCK TABLES `jadwal_pelatihans` WRITE;
/*!40000 ALTER TABLE `jadwal_pelatihans` DISABLE KEYS */;
INSERT INTO `jadwal_pelatihans` VALUES (4,1,2,'2023-12-25 07:26:44','2023-12-25 07:26:44');
/*!40000 ALTER TABLE `jadwal_pelatihans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kehadirans`
--

DROP TABLE IF EXISTS `kehadirans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kehadirans` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_pelatihan` bigint unsigned NOT NULL,
  `id_users` bigint unsigned NOT NULL,
  `id_materi` bigint unsigned NOT NULL,
  `kehadiran` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kehadirans_id_pelatihan_foreign` (`id_pelatihan`),
  KEY `kehadirans_id_users_foreign` (`id_users`),
  KEY `kehadirans_id_materi_foreign` (`id_materi`),
  CONSTRAINT `kehadirans_id_materi_foreign` FOREIGN KEY (`id_materi`) REFERENCES `materis` (`id`) ON DELETE CASCADE,
  CONSTRAINT `kehadirans_id_pelatihan_foreign` FOREIGN KEY (`id_pelatihan`) REFERENCES `pelatihans` (`id`) ON DELETE CASCADE,
  CONSTRAINT `kehadirans_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kehadirans`
--

LOCK TABLES `kehadirans` WRITE;
/*!40000 ALTER TABLE `kehadirans` DISABLE KEYS */;
INSERT INTO `kehadirans` VALUES (4,1,4,4,'hadir','2023-12-26 09:11:57','2023-12-26 09:11:57');
/*!40000 ALTER TABLE `kehadirans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `komentars`
--

DROP TABLE IF EXISTS `komentars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `komentars` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_pelatihan` bigint unsigned NOT NULL,
  `id_users` bigint unsigned NOT NULL,
  `id_materi` bigint unsigned NOT NULL,
  `komentar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `komentars_id_pelatihan_foreign` (`id_pelatihan`),
  KEY `komentars_id_users_foreign` (`id_users`),
  KEY `komentars_id_materi_foreign` (`id_materi`),
  CONSTRAINT `komentars_id_materi_foreign` FOREIGN KEY (`id_materi`) REFERENCES `materis` (`id`) ON DELETE CASCADE,
  CONSTRAINT `komentars_id_pelatihan_foreign` FOREIGN KEY (`id_pelatihan`) REFERENCES `pelatihans` (`id`) ON DELETE CASCADE,
  CONSTRAINT `komentars_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `komentars`
--

LOCK TABLES `komentars` WRITE;
/*!40000 ALTER TABLE `komentars` DISABLE KEYS */;
INSERT INTO `komentars` VALUES (1,1,4,4,'<p>test</p>','2023-12-26 09:28:41','2023-12-26 09:28:41'),(2,1,4,4,'<p>ijin bwertanya</p>','2023-12-26 09:30:37','2023-12-26 09:30:37'),(3,1,12,4,'<p>oke mantap</p>','2023-12-26 09:38:25','2023-12-26 09:38:25'),(4,1,2,4,'<p>iya teman</p>','2023-12-26 09:40:44','2023-12-26 09:40:44'),(5,1,4,4,'<p>ok</p>','2023-12-26 16:58:45','2023-12-26 16:58:45');
/*!40000 ALTER TABLE `komentars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materis`
--

DROP TABLE IF EXISTS `materis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `materis` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_pelatihan` bigint unsigned NOT NULL,
  `id_users` bigint unsigned NOT NULL,
  `nama_materi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `materis_id_pelatihan_foreign` (`id_pelatihan`),
  KEY `materis_id_users_foreign` (`id_users`),
  CONSTRAINT `materis_id_pelatihan_foreign` FOREIGN KEY (`id_pelatihan`) REFERENCES `pelatihans` (`id`) ON DELETE CASCADE,
  CONSTRAINT `materis_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materis`
--

LOCK TABLES `materis` WRITE;
/*!40000 ALTER TABLE `materis` DISABLE KEYS */;
INSERT INTO `materis` VALUES (4,1,2,'Exercitationem irure','Incidunt perferendi','<p>                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Doloribus minus blanditiis reprehenderit, quisquam, provident inventore ducimus perspiciatis, dolores eaque perferendis labore? Quaerat sed molestias aliquid accusantium deserunt eos, soluta natus quam repellendus blanditiis, unde doloremque? Officia iure dolore doloremque reiciendis sint est illo veritatis assumenda? Laborum consectetur eaque, optio aperiam sapiente asperiores? Consequuntur, nulla excepturi. Eaque explicabo dolorem cupiditate iure dolor ullam laborum ipsa perferendis non magnam enim mollitia nesciunt illo, qui consequatur sit harum molestiae atque sequi nemo corrupti unde. Ea exercitationem iste deleniti totam quasi magnam officiis eligendi. Cum dolorum quibusdam expedita fuga repellat accusamus tenetur molestias earum voluptas? Totam nam nulla odit maxime unde id laborum autem explicabo culpa sint iste corrupti, magni, vero nostrum non? Exercitationem voluptates harum iste, aliquam quam quia veniam illo consequatur molestias autem aperiam minima assumenda nulla laborum modi laboriosam optio sit? Ad odit sunt eligendi, quisquam dolorum, explicabo expedita ullam debitis voluptatibus ut a neque. Provident nam animi itaque explicabo, nesciunt voluptatum consequuntur quod, totam iusto nisi mollitia quo eligendi? Quia minus sunt ipsum ipsam, vero quo, ipsa, numquam eos facilis totam eaque vitae esse? Maxime illo necessitatibus accusamus sequi impedit accusantium, reprehenderit corporis rem in unde odio ullam eveniet natus animi optio maiores quidem illum possimus porro voluptates dolorum rerum repellat. Reiciendis, possimus est dicta non harum, vel magni, facilis impedit dignissimos necessitatibus minima tenetur doloremque? Quasi magni hic totam, ratione saepe itaque aliquam nesciunt atque iste sed inventore, possimus rerum neque laborum quae facilis deleniti voluptatem aspernatur eum voluptas laboriosam. Ea odit tenetur voluptate ratione est optio dignissimos. Iste iure eum consequatur maiores recusandae nobis quam, labore aperiam dolores a perspiciatis neque ab explicabo praesentium similique dolor ut nesciunt mollitia inventore. Reprehenderit totam explicabo deleniti, illo dolorum, a beatae cumque in ex iusto amet eligendi. Officiis magni nihil eligendi iste rem itaque excepturi ullam vel sequi nam quam fugiat, quos explicabo minima qui est. Dicta porro accusantium facere tempora ea sed quidem cumque atque iure error animi, corrupti, optio necessitatibus rerum! Quasi maiores aliquid veniam similique eaque doloremque voluptatibus obcaecati quaerat, labore autem natus accusamus at harum. Soluta modi recusandae praesentium, quasi error beatae necessitatibus exercitationem facilis autem labore impedit iusto nihil quisquam, voluptatibus dolor, tenetur ex harum doloribus fuga ipsa vero? Nostrum ad commodi, minima voluptas nisi ratione laborum animi dicta provident dolor ex ut nobis omnis fugit tenetur inventore maiores explicabo soluta a eos temporibus qui.</p>\r\n<div style=\"max-width: 650px;\" data-ephox-embed-iri=\"https://www.youtube.com/watch?v=YGGoZbzjiSc\">\r\n<div style=\"left: 0; width: 100%; height: 0; position: relative; padding-bottom: 56.25%;\"><iframe style=\"top: 0; left: 0; width: 100%; height: 100%; position: absolute; border: 0;\" src=\"https://www.youtube.com/embed/YGGoZbzjiSc?rel=0\" scrolling=\"no\" allow=\"accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share;\" allowfullscreen=\"allowfullscreen\"></iframe></div>\r\n</div>','1703517398_kelola_hasil_print2 (20).pdf','2023-12-25 08:16:38','2023-12-25 08:16:38'),(5,1,2,'Magna labore est ea','Aut quasi id id magn','<p>ASDSDSD</p>','1703517421_kelola_hasil_print2 (26).pdf','2023-12-25 08:17:01','2023-12-25 08:17:01');
/*!40000 ALTER TABLE `materis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_12_14_000001_create_personal_access_tokens_table',1),(6,'2023_12_07_130726_create_pelatihans_table',3),(7,'2023_12_07_130817_create_materis_table',3),(9,'2023_12_07_131804_create_komentars_table',3),(10,'2023_12_07_131953_create_dokumen_pendukungs_table',3),(11,'2023_12_04_153634_create_jadwal_pelatihans_table',4),(12,'2023_12_26_055406_create_user_regis_pelatihans_table',5),(14,'2023_12_26_154950_add_materi_id_to_kehadiran_table',6),(15,'2023_12_07_131359_create_kehadirans_table',7);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pelatihans`
--

DROP TABLE IF EXISTS `pelatihans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pelatihans` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_pelatihan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pelatihans`
--

LOCK TABLES `pelatihans` WRITE;
/*!40000 ALTER TABLE `pelatihans` DISABLE KEYS */;
INSERT INTO `pelatihans` VALUES (1,'Pelatihan Membuat Gorengan 4','Minima recusandae E','2023-12-17','2023-12-17 06:29:23','2023-12-17 06:31:11'),(3,'Sade Irwin','Laborum aliquam mole','2022-09-25','2023-12-17 06:31:25','2023-12-17 06:31:25');
/*!40000 ALTER TABLE `pelatihans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_regis_pelatihans`
--

DROP TABLE IF EXISTS `user_regis_pelatihans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_regis_pelatihans` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_pelatihan` bigint unsigned NOT NULL,
  `id_users` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_regis_pelatihans_id_pelatihan_foreign` (`id_pelatihan`),
  KEY `user_regis_pelatihans_id_users_foreign` (`id_users`),
  CONSTRAINT `user_regis_pelatihans_id_pelatihan_foreign` FOREIGN KEY (`id_pelatihan`) REFERENCES `pelatihans` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_regis_pelatihans_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_regis_pelatihans`
--

LOCK TABLES `user_regis_pelatihans` WRITE;
/*!40000 ALTER TABLE `user_regis_pelatihans` DISABLE KEYS */;
INSERT INTO `user_regis_pelatihans` VALUES (1,1,4,'2023-12-25 23:00:57','2023-12-25 23:00:57'),(5,1,12,'2023-12-26 09:38:07','2023-12-26 09:38:07');
/*!40000 ALTER TABLE `user_regis_pelatihans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` bigint NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'Uchiha madara','mentor@mentor.com',NULL,'$2y$10$RGj0byg606GwUhmxAKLPOuiajWDVY84rG81wS/MgKtCk.s0isXzmO','mentor',1,NULL,'2023-12-04 05:50:12','2023-12-04 05:50:12'),(3,'Admin','admin@admin.com',NULL,'$2y$10$Rg5ZFoCDlE8kRar.j7pkl.2lmiczT9yxzTbQvioxMz.vl7bCCejJG','admin',0,NULL,'2023-12-04 05:50:12','2023-12-04 05:50:12'),(4,'Nathaniel Monroe','user@user.com',NULL,'$2y$10$iS3sLPC59jCk/lxiWPf4CesxptDJuaNClIzznXGLhSuXHH046rWai','user',2,NULL,'2023-12-04 08:07:19','2023-12-17 10:33:33'),(9,'mentor1','mentor1@mentor.com',NULL,'$2y$10$i/tjMexeFGFwurrcCmEvmepasZUJqa5.z7G0Qu4qlhK7gmuU8NDZ6','mentor',1,NULL,'2023-12-25 08:07:21','2023-12-25 08:07:21'),(10,'mentor2','mentor2@mentor.com',NULL,'$2y$10$2G82HYLqTuBRQFlgFsZnpedxQwZQDX2IEZXUkQeoFkYZ3twLjausK','mentor',1,NULL,'2023-12-25 08:07:21','2023-12-25 08:07:21'),(11,'mentor3','mentor3@mentor.com',NULL,'$2y$10$xOn/PWfDA2WN1DhMxxYkCuxQydsMB/GWnW.vq1pzfWCZ2exQ.CHtK','mentor',1,NULL,'2023-12-25 08:07:21','2023-12-25 08:07:21'),(12,'JeniLf','jeni@user.com',NULL,'$2y$10$iS3sLPC59jCk/lxiWPf4CesxptDJuaNClIzznXGLhSuXHH046rWai','user',2,NULL,'2023-12-04 08:07:19','2023-12-17 10:33:33');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'pelatihan_umkm'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-12-28 19:18:49
