<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class MentorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'mentor1',
                'email' => 'mentor1@mentor.com',
                'password' => Hash::make('mentor1'),
                'jabatan' => 'mentor',
                'role' => 1

            ],
            [
                'name' => 'mentor2',
                'email' => 'mentor2@mentor.com',
                'password' => Hash::make('mentor'),
                'jabatan' => 'mentor',
                'role' => 1

            ],
            [
                'name' => 'mentor3',
                'email' => 'mentor3@mentor.com',
                'password' => Hash::make('mentor3'),
                'jabatan' => 'mentor',
                'role' => 1
            ]
        ];
        foreach ($users as $user) {
            \App\Models\User::create($user);
        }
    }
}
