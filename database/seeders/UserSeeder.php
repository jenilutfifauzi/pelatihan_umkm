<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
                    [
                        'name' => 'user',
                        'email' => 'user@user.com',
                        'password' => Hash::make('user'),
                        'jabatan' => 'user',
                        'role' => 2

                    ],
                    [
                        'name' => 'mentor',
                        'email' => 'mentor@mentor.com',
                        'password' => Hash::make('mentor'),
                        'jabatan' => 'mentor',
                        'role' => 1

                    ],
                    [
                        'name' => 'Admin',
                        'email' => 'admin@admin.com',
                        'password' => Hash::make('admin'),
                        'jabatan' => 'admin',
                        'role' => 0
                    ]
                    ];
        foreach ($users as $user) {
            \App\Models\User::create($user);
        }
    }
}
