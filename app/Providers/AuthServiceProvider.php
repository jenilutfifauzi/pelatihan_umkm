<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //admin
        Gate::define('isAdmin', function ($user) {
            return $user->role == 0;
        });
        //mentor
        Gate::define('isMentor', function ($user) {
            return $user->role == 1;
        });
        //user
        Gate::define('isUser', function ($user) {
            return $user->role == 2;
        });
    }
}
