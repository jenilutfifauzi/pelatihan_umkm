<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JadwalPelatihan extends Model
{
    use HasFactory;

    protected $table = 'jadwal_pelatihans';
    protected $guarded = [];

    public function pelatihan()
    {
        return $this->belongsTo(Pelatihan::class, 'id_pelatihan', 'id');
    }

    public function mentor()
    {
        return $this->belongsTo(User::class, 'id_user', 'id');
    }
}
