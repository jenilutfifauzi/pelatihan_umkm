<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pelatihan extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'deskripsi',
        'tanggal_pelatihan'
    ];
    protected $table = 'pelatihans';

    public function mentor()
    {
        return $this->belongsTo(User::class, 'id_user', 'id');
    }
}
