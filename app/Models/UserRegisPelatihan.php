<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRegisPelatihan extends Model
{
    use HasFactory;

    protected $table = 'user_regis_pelatihans';
    protected $guarded = [];
}
