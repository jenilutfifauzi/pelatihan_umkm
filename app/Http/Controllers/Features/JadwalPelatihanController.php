<?php

namespace App\Http\Controllers\Features;

use App\Http\Controllers\Controller;
use App\Models\JadwalPelatihan;
use App\Models\Pelatihan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JadwalPelatihanController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $data = JadwalPelatihan::with('pelatihan', 'mentor')->get();
            return \DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('pelatihan', function ($row) {
                    return $row->pelatihan->name;
                })
                ->addColumn('mentor', function ($row) {
                    return $row->mentor->name;
                })
                ->addColumn('action', function ($row) {
                    $action = ' <form action="' . route('jadwal.destroy', $row->id) . '" method="POST" class="d-inline">';
                    $action .= csrf_field();
                    $action .= method_field('DELETE');
                    $action .= ' <button type="submit" class="btn btn-danger btn-sm btn-delete">Delete</button>';
                    $action .= '</form>';

                    return $action;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('jadwal_pelatihan.index');
    }

    public function create()
    {
        $mentor = User::where('jabatan', 'mentor')->get();
        $pelatihan = Pelatihan::all();
        return view('jadwal_pelatihan.create', compact('mentor', 'pelatihan'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'id_pelatihan' => 'required|unique:jadwal_pelatihans,id_pelatihan',
            'id_user' => 'required',
        ]);

        JadwalPelatihan::create($request->all());

        return redirect()->route('jadwal.index')
            ->with('success', 'Pelatihan created successfully.');
    }

    public function destroy($id)
    {
        JadwalPelatihan::where('id', $id)->delete();

        return redirect()->route('jadwal.index')
            ->with('success', 'Pelatihan deleted successfully');
    }
}
