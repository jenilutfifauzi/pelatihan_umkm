<?php

namespace App\Http\Controllers\Features;

use App\Http\Controllers\Controller;
use App\Models\JadwalPelatihan;
use App\Models\Kehadiran;
use App\Models\Komentar;
use App\Models\Materi;
use App\Models\Pelatihan;
use App\Models\UserRegisPelatihan;
use Illuminate\Http\Request;

class MateriUserController extends Controller
{
    public function index()
    {
        //CEK PELATIHAN USER
        $cek = UserRegisPelatihan::where('id_users', auth()->user()->id)->get();
        if ($cek->isEmpty()) {
            return back()->with('error', 'Anda belum mendaftar pelatihan');
        }
        //tampilkan pelatihan berdasarkan user regis pelatihan
        $pelatihan = Pelatihan::whereIn('id', $cek->pluck('id_pelatihan'))->get();
        return view('materi_user.index', compact('pelatihan'));
    }

    public function show($id)
    {
        //CEK PELATIHAN USER
        $cek = UserRegisPelatihan::where('id_users', auth()->user()->id)->get();
        if ($cek->isEmpty()) {
            return back()->with('error', 'Anda belum mendaftar pelatihan');
        }
        //tampilkan pelatihan berdasarkan user regis pelatihan
        $pelatihan = Pelatihan::where('id', $id)->first();
        if (!$pelatihan) {
            return back()->with('error', 'Anda belum mendaftar pelatihan');
        }
        //data
        if (request()->ajax()) {
            $data = Materi::with('pelatihan')->where('id_pelatihan', $id)->get();
            return \DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('nama_pelatihan', function ($row) {
                    return $row->pelatihan->name ?? '';
                })
                ->addColumn('nama_materi', function ($row) {
                    return $row->nama_materi;
                })
                ->addColumn('action', function ($row) {
                    $action = ' <a class="btn btn-info btn-sm" href="' . route('materi_user.detail', $row->id) . '">Detail</a>';
                    
                    return $action;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $materis = Materi::where('id_pelatihan', $id)->get();
        return view('materi_user.show', compact('pelatihan', 'materis', 'id'));
    }

    public function detail($id)
    {
        $materi = Materi::where('id', $id)->first();
        //cari jadwal pelatihan
        $data_pelatihan = JadwalPelatihan::with('pelatihan', 'mentor')->where('id_pelatihan', $materi->id_pelatihan)->first();
        //cek absen materi
        $cek_kehadiran = Kehadiran::where('id_users', auth()->user()->id)->where('id_materi', $id)->first();
        //comments
        $comments = Komentar::with('user')->where('id_materi', $id)->get();
        return view('materi_user.detail', compact('materi', 'data_pelatihan', 'cek_kehadiran', 'comments'));
    }
}
