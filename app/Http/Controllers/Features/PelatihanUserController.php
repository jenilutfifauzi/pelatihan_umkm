<?php

namespace App\Http\Controllers\Features;

use App\Http\Controllers\Controller;
use App\Models\Pelatihan;
use App\Models\User;
use App\Models\UserRegisPelatihan;
use Illuminate\Http\Request;

class PelatihanUserController extends Controller
{
    public function index()
    {
        $pelatihan = Pelatihan::all();
        return view('pelatihan_user.index', compact('pelatihan'));
    }

    public function user_register(Request $request, $id)
    {
        //validasi
        $request->validate([
            'id_pelatihan' => 'required',
        ]);
        //cek apakah sudah daftar
        $cek = UserRegisPelatihan::where('id_pelatihan', $id)->where('id_users', auth()->user()->id)->first();
        if ($cek) {
            return back()->with('error', 'Anda sudah mendaftar pelatihan ini');
        }
        //simpan ke user_regis_pelatihan
        UserRegisPelatihan::create([
            'id_pelatihan' => $id,
            'id_users' => auth()->user()->id
        ]);
        return back()->with('success', 'Berhasil mendaftar pelatihan');
    }
}
