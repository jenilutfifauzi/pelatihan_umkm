<?php

namespace App\Http\Controllers\Features;

use App\Http\Controllers\Controller;
use App\Models\Kehadiran;
use Illuminate\Http\Request;

class KehadiranUserController extends Controller
{
    public function store(Request $request)
    {
        
        $request->validate([
            'id_users' => 'required',
            'id_pelatihan' => 'required',
            'id_materi' => 'required',
        ]);

        $data = [
            'id_users' => $request->id_users,
            'id_pelatihan' => $request->id_pelatihan,
            'id_materi' => $request->id_materi,
            'kehadiran' => 'hadir'
        ];

        $kehadiran = Kehadiran::create($data);

        return redirect()->back()->with('success', 'Kehadiran created successfully.');
    }
}
