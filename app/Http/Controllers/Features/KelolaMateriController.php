<?php

namespace App\Http\Controllers\Features;

use App\Http\Controllers\Controller;
use App\Models\JadwalPelatihan;
use App\Models\Materi;
use App\Models\Pelatihan;
use Illuminate\Http\Request;

class KelolaMateriController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $data = Materi::with('pelatihan')->where('id_users', auth()->user()->id)->get();
            return \DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('nama_pelatihan', function ($row) {
                    return $row->pelatihan->name;
                })
                ->addColumn('action', function ($row) {
                    $action = ' <form action="' . route('kelola_materi.destroy', $row->id) . '" method="POST" class="d-inline">';
                    $action .= csrf_field();
                    $action .= method_field('DELETE');
                    $action .= ' <button type="submit" class="btn btn-danger btn-sm btn-delete">Delete</button>';
                    $action .= '</form>';

                    $action .= ' <a class="btn btn-info btn-sm" href="' . route('materi_user.detail', $row->id) . '">Detail</a>';

                    return $action;
                })
                ->rawColumns(['action', 'nama_pelatihan'])
                ->make(true);
        }
        return view('kelola_materi.index');
    }

    public function create()
    {
        $jadwal = JadwalPelatihan::with('pelatihan')->where('id_user', auth()->user()->id)->get();
        $pelatihan = Pelatihan::all();
        return view('kelola_materi.create', compact('pelatihan', 'jadwal'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'id_pelatihan' => 'required',
            'nama_materi' => 'required',
            'deskripsi' => 'required',
            'body' => 'required',
            'file' => 'required|mimes:pdf|max:2048',
        ]);

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $nama_file = time() . "_" . $file->getClientOriginalName();
            $tujuan_upload = 'file_materi';
            $file->move($tujuan_upload, $nama_file);
        } else {
            $nama_file = null;
        }

        $materi = new Materi();
        $materi->id_pelatihan = $request->id_pelatihan;
        $materi->id_users = auth()->user()->id;
        $materi->nama_materi = $request->nama_materi;
        $materi->deskripsi = $request->deskripsi;
        $materi->body = $request->body;
        $materi->file = $nama_file;
        $materi->save();

        return redirect()->route('kelola_materi.index')->with('success', 'Materi berhasil ditambahkan');
    }

    public function destroy($id)
    {
        Materi::where('id', $id)->delete();

        return redirect()->route('kelola_materi.index')
            ->with('success', 'Materi deleted successfully');
    }
}
