<?php

namespace App\Http\Controllers\Features;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class KelolaUserController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $user = User::all();
            return \DataTables::of($user)
                ->addIndexColumn()

                ->addColumn('action', function ($row) {
                    $action = ' <a class="btn btn-primary btn-sm" href="' . route('kelola_user.edit', $row->id) . '">Edit</a>';
                    $action .= ' <form action="' . route('kelola_user.destroy', $row->id) . '" method="POST" class="d-inline">';
                    $action .= csrf_field();
                    $action .= method_field('DELETE');
                    $action .= ' <button type="submit" class="btn btn-danger btn-sm btn-delete">Delete</button>';
                    $action .= '</form>';

                    return $action;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('kelola_user.index');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('kelola_user.edit', compact('user'));
    }

    public function update (Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
        ]);
        $user = User::findOrFail($id);
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'tgl_lahir' => $request->tgl_lahir,
            'alamat' => $request->alamat,
        ];
        if ($request->password) {
            $validated = $request->validate([
                'password' => 'required|min:8',
            ]);
            $data['password'] = bcrypt($request->password);
        }
        $user->update($data);

        return redirect()->route('kelola_user.index')->with('success', 'Data berhasil diupdate');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->back()->with('success', 'Data berhasil dihapus');
    }
}
