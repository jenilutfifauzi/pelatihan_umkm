<?php

namespace App\Http\Controllers\Features;

use App\Http\Controllers\Controller;
use App\Models\Komentar;
use Illuminate\Http\Request;

class KomentarUserController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'id_pelatihan' => 'required',
            'id_users' => 'required',
            'id_materi' => 'required',
            'komentar' => 'required',
        ]);

        $komentar = Komentar::create([
            'id_pelatihan' => $request->id_pelatihan,
            'id_users' => auth()->user()->id,
            'id_materi' => $request->id_materi,
            'komentar' => $request->komentar,
        ]);

        return back()->with('success', 'Komentar berhasil ditambahkan');
    }
}
