<?php

namespace App\Http\Controllers\Features;

use App\Http\Controllers\Controller;
use App\Models\Pelatihan;
use Illuminate\Http\Request;

class PelatihanController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $data = Pelatihan::all();
            return \DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = ' <a class="btn btn-primary btn-sm" href="' . route('pelatihan.edit', $row->id) . '">Edit</a>';
                    $action .= ' <form action="' . route('pelatihan.destroy', $row->id) . '" method="POST" class="d-inline">';
                    $action .= csrf_field();
                    $action .= method_field('DELETE');
                    $action .= ' <button type="submit" class="btn btn-danger btn-sm btn-delete">Delete</button>';
                    $action .= '</form>';

                    return $action;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('pelatihan.index');
    }

    public function create()
    {
        return view('pelatihan.create');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'deskripsi' => 'required',
            'tanggal_pelatihan' => 'required',
        ]);
        $data = [
            'name' => $request->name,
            'deskripsi' => $request->deskripsi,
            'tanggal_pelatihan' => $request->tanggal_pelatihan
        ];
        Pelatihan::create($data);
        return redirect()->route('pelatihan.index')->with('success', 'Pelatihan berhasil ditambahkan');
    }

    public function edit($id)
    {
        $pelatihan = Pelatihan::findOrFail($id);
        return view('pelatihan.edit', compact('pelatihan'));
    }

    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required',
            'deskripsi' => 'required',
            'tanggal_pelatihan' => 'required',
        ]);
        $pelatihan = Pelatihan::findOrFail($id);
        $data = [
            'name' => $request->name,
            'deskripsi' => $request->deskripsi,
            'tanggal_pelatihan' => $request->tanggal_pelatihan
        ];
        $pelatihan->update($data);
        return redirect()->route('pelatihan.index')->with('success', 'Pelatihan berhasil diupdate');
    }

    public function destroy($id)
    {
        $pelatihan = Pelatihan::findOrFail($id);
        $pelatihan->delete();
        return redirect()->route('pelatihan.index')->with('success', 'Pelatihan berhasil dihapus');
    }
}
