@extends('layouts.app-main')

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tambah Data Materi</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('kelola_materi.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        
                        <div class="form-group row">
                            <label for="inputNama" class="col-sm-3 col-form-label">Nama Pelatihan</label>
                            <div class="col-sm-9">
                                <select class="form-control @error('id_pelatihan')
                                is-invalid
                                @enderror" name="id_pelatihan" id="inputNama">
                                    <option value="">Pilih Pelatihan</option>
                                    @foreach ($jadwal as $item)
                                        <option value="{{ $item->pelatihan ? $item->pelatihan->id : '' }}">{{ $item->pelatihan ? $item->pelatihan->name : '' }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('id_pelatihan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                       
                        <div class="form-group row">
                            <label for="inputTgl" class="col-sm-3 col-form-label">Judul Materi</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control @error('nama_materi') is-invalid @enderror" name="nama_materi" id="inputTgl" placeholder="Judul Materi">
                            </div>
                            @error('nama_materi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <label for="inputTgl" class="col-sm-3 col-form-label">Deskripsi Singkat</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control @error('deskripsi') is-invalid @enderror" name="deskripsi" id="inputTgl" placeholder="Deskripsi Singkat">
                            </div>
                            @error('deskripsi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <label for="inputTgl" class="col-sm-3 col-form-label">Isi Materi</label>
                            <div class="col-sm-9">
                                <textarea class="form-control body-materi @error('body') is-invalid @enderror" name="body" cols="30" rows="10"></textarea>
                            </div>
                            @error('body')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <label for="inputTgl" class="col-sm-3 col-form-label">File Pendukung</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control @error('file') is-invalid @enderror" name="file">
                            </div>
                            @error('file')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('script')
    @endpush
@endsection
