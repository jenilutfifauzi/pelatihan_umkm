@extends('layouts.app-main')

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Edit Data User</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('kelola_user.update', $user->id) }}" method="post">
                        @csrf
                        @method('PUT')
                        
                        <div class="form-group row">
                            <label for="inputNama" class="col-sm-3 col-form-label">Nama</label>
                            <div class="col-sm-9">
                                <input type="text"
                                    class="form-control @error('nip')
                                    is-invalid
                                    @enderror"
                                    value="{{ old('name', $user->name) }}"
                                    @error('name')
                                    autofocus
                                    @enderror
                                    id="inputNama" name="name" placeholder="Nama">
                            </div>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email"
                                    class="form-control @error('email')
                                    is-invalid
                                    @enderror"
                                    value="{{ old('email', $user->email) }}"
                                    @error('email')
                                    autofocus
                                    @enderror
                                    id="inputEmail3" name="email" placeholder="Email">
                            </div>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="inputTgl" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                            <div class="col-sm-9">
                                <input type="date"
                                    class="form-control @error('tgl_lahir')
                                    is-invalid
                                    @enderror"
                                    value="{{ old('tgl_lahir', $user->tgl_lahir) }}"
                                    @error('tgl_lahir')
                                    autofocus
                                    @enderror
                                    id="inputTgl" name="tgl_lahir" placeholder="Tanggal lahir" >
                            </div>
                            @error('tgl_lahir')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="inputAlamat" class="col-sm-3 col-form-label">Alamat</label>
                            <div class="col-sm-9">
                                <input type="textarea"
                                    class="form-control @error('alamat')
                                    is-invalid
                                    @enderror"
                                    value="{{ old('alamat', $user->alamat) }}"
                                    @error('alamat')
                                    autofocus
                                    @enderror
                                    id="inputAlamat" name="alamat" placeholder="Alamat" >
                            </div>
                            @error('alamat')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-3 col-form-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="inputPassword3" name="password"
                                    placeholder="Password">
                            </div>
                        </div>
                        

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('script')
    @endpush
@endsection
