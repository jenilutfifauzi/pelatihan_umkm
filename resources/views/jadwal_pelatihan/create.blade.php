@extends('layouts.app-main')

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Edit Data User</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('jadwal.store') }}" method="post">
                        @csrf
                        
                        <div class="form-group row">
                            <label for="inputNama" class="col-sm-3 col-form-label">Nama Pelatihan</label>
                            <div class="col-sm-9">
                                <select class="form-control @error('name')
                                is-invalid
                                @enderror" name="id_pelatihan" id="inputNama">
                                    <option value="">Pilih Pelatihan</option>
                                    @foreach ($pelatihan as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('id_pelatihan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                       
                        <div class="form-group row">
                            <label for="inputTgl" class="col-sm-3 col-form-label">Mentor</label>
                            <div class="col-sm-9">
                                <select class="form-control @error('id_user')
                                is-invalid
                                @enderror" name="id_user" id="inputTgl">
                                    <option value="">Pilih Mentor</option>
                                    @foreach ($mentor as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('id_user')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('script')
    @endpush
@endsection
