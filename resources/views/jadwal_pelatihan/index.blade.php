@extends('layouts.app-main')

@section('content')
    <div class="row">
        <div class="col-12 stretch-card transparent">
            <div class="card">
                <div class="card-header">{{ __('Kelola Data Jadwal Mentor') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Data Jadwal Mentor</h6>
                    </div>
                    <div class="table-responsive p-3">
                        <a href="{{ route('jadwal.create')}}" class="btn btn-primary"> Tambah Jadwal Pelatihan Mentor</a>
                        <table class="table align-items-center table-flush" id="dataTable">
                            <thead class="thead-light">
                                <tr>
                                    <th>No</th>
                                    <th>Pelatihan</th>
                                    <th>Mentor</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push("script")
    <script>
        $(document).ready(function() {
            var table = $('#dataTable').DataTable({
                processing: true,
                ajax: {
                    url: "{{ route('jadwal.index') }}",
                },
                serverSide: true,
                columnDefs: [{
                    "defaultContent": "-",
                    "targets": "_all"
                }, ],
                columns: [{
                        data: 'id',
                        name: 'id',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'pelatihan',
                        name: 'pelatihan'
                    },
                    {
                        data: 'mentor',
                        name: 'mentor'
                    },
                    {
                        data: 'action',
                    }

                ],
                dom: 'Bfrtip',
                    buttons: [
                        'pdf','excel', 'print',
                    ]
            });
        });
    </script>

    @endpush
@endsection
