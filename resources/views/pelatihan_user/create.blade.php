@extends('layouts.app-main')

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tambah Data Pelatihan</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('pelatihan.store') }}" method="post">
                        @csrf
                        
                        <div class="form-group row">
                            <label for="inputNama" class="col-sm-3 col-form-label">Nama Pelatihan</label>
                            <div class="col-sm-9">
                                <input type="text"
                                    class="form-control @error('name')
                                    is-invalid
                                    @enderror"
                                    
                                    @error('name')
                                    autofocus
                                    @enderror
                                    id="inputNama" name="name" placeholder="Nama">
                            </div>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="deskripsi" class="col-sm-3 col-form-label">Deskripsi</label>
                            <div class="col-sm-9">
                                <input type="deskripsi"
                                    class="form-control @error('deskripsi')
                                    is-invalid
                                    @enderror"

                                    @error('deskripsi')
                                    autofocus
                                    @enderror
                                    id="inputdeskripsi3" name="deskripsi" placeholder="deskripsi">
                            </div>
                            @error('deskripsi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="inputTgl" class="col-sm-3 col-form-label">Tanggal Pelatihan</label>
                            <div class="col-sm-9">
                                <input type="date"
                                    class="form-control @error('tanggal_pelatihan')
                                    is-invalid
                                    @enderror"
                                    
                                    @error('tanggal_pelatihan')
                                    autofocus
                                    @enderror
                                    id="inputTgl" name="tanggal_pelatihan" placeholder="Tanggal Pelatihan" >
                            </div>
                            @error('tanggal_pelatihan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('script')
    @endpush
@endsection
