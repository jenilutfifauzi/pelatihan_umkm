@extends('layouts.app-main')

@section('content')
    <div class="row">
        <div class="col-12 stretch-card transparent">
            <div class="card">
                <div class="card-header">{{ __('Daftar Pelatihan') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-md-12 grid-margin transparent">
                        <div class="row">
                            @foreach ($pelatihan as $item)
                            <div class="col-md-6 mb-4 stretch-card transparent">
                                <div class="card card-tale">
                                    <div class="card-body">
                                        <p class="mb-4">Pelatihan</p>
                                        <p class="fs-30 mb-2">{{ $item->name ?? '' }}</p>
                                        <p>{{ $item->tanggal_pelatihan ?? '' }}</p>
                                        
                                        <form action="{{ route('pelatihan_user.user_register', $item->id) }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="id_pelatihan" value="{{ $item->id }}">
                                            <button type="submit" class="btn btn-primary">Klik untuk mendaftar(abaikan jika sudah mendaftar)</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('script')
        <script>
            $(document).ready(function() {
                var table = $('#dataTable').DataTable({
                    processing: true,
                    ajax: {
                        url: "{{ route('pelatihan.index') }}",
                    },
                    serverSide: true,
                    columnDefs: [{
                        "defaultContent": "-",
                        "targets": "_all"
                    }, ],
                    columns: [{
                            data: 'id',
                            name: 'id',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'deskripsi',
                            name: 'deskripsi'
                        },
                        {
                            data: 'tanggal_pelatihan',
                            name: 'tanggal_pelatihan'
                        },
                        {
                            data: 'action',
                        }

                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        'pdf', 'excel', 'print',
                    ]
                });
            });
        </script>
    @endpush
@endsection
