@extends('layouts.app-main')

@section('content')
    <div class="row">
        <div class="col-12 stretch-card transparent">
            <div class="card">
                <div class="card-header">{{ __('Daftar Materi') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="table-responsive ">
                        <table class="table align-items-center table-flush" id="dataTable">
                            <thead class="thead-light">
                                <tr>
                                    <th>No</th>
                                    <th>Nama Pelatihan</th>
                                    <th>Nama Materi</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('script')
        <script>
            $(document).ready(function() {
                var table = $('#dataTable').DataTable({
                    processing: true,
                    ajax: {
                        url: "{{ route('materi_user.show', $id) }}",
                    },
                    serverSide: true,
                    columnDefs: [{
                        "defaultContent": "-",
                        "targets": "_all"
                    }, ],
                    columns: [{
                            data: 'id',
                            name: 'id',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelatihan',
                            name: 'nama_pelatihan'
                        },
                        {
                            data: 'nama_materi',
                            name: 'nama_materi'
                        },
                        {
                            data: 'action',
                        }

                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        'pdf', 'excel', 'print',
                    ]
                });
            });
        </script>
    @endpush
@endsection
