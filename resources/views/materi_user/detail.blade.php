@extends('layouts.app-main')

@section('content')
    <div class="row">
        <div class="col-12 stretch-card transparent">
            <div class="card">
                <div class="card-header">{{ __('Daftar Materi') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-md-12 grid-margin transparent">
                        <div class="">
                            <h4>{{ $materi->nama_materi ?? '' }}</h4>
                            <h5>Mentor: {{ $data_pelatihan->mentor->name ?? '' }}</h5>
                            <h5>Pelatihan: {{ $data_pelatihan->pelatihan->name ?? '' }}</h5>
                            <h5>Tanggal Pelatihan: {{ $data_pelatihan->pelatihan->tanggal_pelatihan ?? '' }}</h5>
                        </div>
                        <hr>
                        <div class="isi-body">
                            <p>{!! $materi->body ?? '' !!}</p>
                        </div>
                        <hr>
                        <div class="file-download">
                            <p>File Materi: </p>
                            <a href="{{ url('file_materi/' . $materi->file) }}" class="btn btn-primary"
                                target="_blank">Download</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        {{-- absen --}}
        <div class="col-12 stretch-card transparent mt-4">
            <div class="card">
                <div class="card-body">
                    @if ($cek_kehadiran == null && auth()->user()->role == 2)
                        <h4>Anda belum absen</h4>
                        <p>Silahkan absen terlebih dahulu</p>
                        <form action="{{ route('kehadiran_user.store') }}" method="POST">
                            @csrf
                            <input type="hidden" name="id_pelatihan" value="{{ $data_pelatihan->pelatihan->id }}">
                            <input type="hidden" name="id_materi" value="{{ $materi->id }}">
                            <input type="hidden" name="id_users" value="{{ Auth::user()->id }}">
                            <button type="submit" class="btn btn-primary">Absen</button>
                        </form>
                    @elseif($cek_kehadiran != null && auth()->user()->role == 2)
                        <h4>Anda sudah absen</h4>
                        <p>Terima kasih telah mengikuti pelatihan ini</p>
                    @endif
                </div>
            </div>
        </div>
        {{-- Komentar --}}
        <div class="col-12 mt-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-2">Komentar</h4>
                    <hr>
                    <!-- Daftar komentar -->
                    <div class="comments">
                        @foreach ($comments as $comment)
                            <div class="comment">
                                <div class="comment-header">
                                    <strong>{{ $comment->user->name }}</strong>
                                    @if (auth()->user()->id == $comment->id_users && auth()->user()->role == 2)
                                        <span class="badge badge-info">You</span>
                                    @elseif(auth()->user()->id == $comment->id_users && auth()->user()->role == 1)
                                        <span class="badge badge-danger">Mentor</span>
                                    @endif
                                    @if ($comment->user->role == 1)
                                        <span class="badge badge-danger">Mentor</span>
                                    @endif
                                    <small class="text-muted">{{ $comment->created_at->format('d M Y H:i') }}</small>
                                </div>
                                <p>{!! $comment->komentar !!}</p>
                            </div>
                        @endforeach
                    </div>

                    <!-- Form komentar -->
                    <hr>
                    <h5 class="mt-4">Tulis Komentar</h5>
                    <form action="{{ route('komentar_user.store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id_pelatihan" value="{{ $data_pelatihan->pelatihan->id }}">
                        <input type="hidden" name="id_materi" value="{{ $materi->id }}">
                        <input type="hidden" name="id_users" value="{{ Auth::user()->id }}">
                        <div class="form-group">
                            <textarea class="form-control komentar" name="komentar" rows="4" placeholder="Tulis komentar Anda"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Kirim Komentar</button>
                    </form>
                </div>
            </div>
        </div>


    </div>
    @push('script')
        <script>
            $(document).ready(function() {
                tinymce.init({
                    selector: '.komentar',
                    plugins: '',
                    toolbar: '',
                    menubar: false,
                    ai_request: (request, respondWith) => respondWith.string(() => Promise.reject(
                        "See docs to implement AI Assistant")),
                });
            });
        </script>
    @endpush
@endsection
